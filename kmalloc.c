#include <sys/types.h>
#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/sysproto.h>

static int hello(struct thread *p, void *arg)
{
	uprintf("hello kernel\n");
	return 0;
}

static struct sysent hello_sysent = {
0, //number of parameters
hello //name of function
};

static int offset = NO_SYSCALL; //sets offset to syscall end

static int load(struct module *module, int cmd, void *arg) //standard for modules
{
int error = 0;
	switch(cmd)
	{
	case MOD_LOAD:
		uprintf("syscall!");
		break;
	case MOD_UNLOAD:
		uprintf("syscall unloaded");
		break;
	default:
		error = EINVAL;
		break;
	}
	return error;
}

SYSCALL_MODULE(syscall,&offset, &hello_sysent, load, NULL);


