#include <sys/types.h>
#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysent.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/sysproto.h>
#include <sys/vmem.h>

#define KMALLOC 0
#define KFREE 1

vmem_t *myvm;
vmem_addr_t addrp;
vmem_addr_t addrOld;

#ifndef _SYS_SYS_PROTO_H_
struct kmalloc_args
{
	int cmd;
	size_t size;
	vmem_addr_t addrp;
};
#endif

static int kmalloc(struct thread *p, void *arg)
{	
	struct kmalloc_args *args = arg;
	
	int cmd = (args->cmd);
	addrp = (args->addrp);
	size_t size = (args->size);
	
//	size = round_page(size); //Set to page in kern_init.
//	vmem_add(myvmem, addrp+100, size, M_NOWAIT);

	switch(cmd)
	{
        case KMALLOC:
		vmem_alloc(myvm,size ,M_FIRSTFIT| M_NOWAIT ,&addrp );
		printf("Allocated Memory on: %04x \n", (unsigned int) addrp);
		addrOld = addrp;
		break;		
	case KFREE:
	if(addrOld + size > 0)
	{
		vmem_free(myvm, addrOld, size);
		addrOld -= size;
		printf("Unallocated Chunk of Size: %04x\n", (unsigned int) size);
		break;
	}
	default:
		printf("No CMD!");
		break;
	}
	return 0;
}

static struct sysent kmalloc_sysent = {
3, //number of parameters
kmalloc //name of function
};

static int offset = NO_SYSCALL; //sets offset to syscall end

static int load(struct module *module, int cmd, void *arg) //standard for modules
{
int error = 0;
	switch(cmd)
	{
	case MOD_LOAD:
	  myvm = vmem_create("Kernel2", 0, 1000, 1,0, M_FIRSTFIT);
	  uprintf("Virtual Memory Created!\n");
	  break;
	case MOD_UNLOAD:
	  vmem_destroy(myvm);
	  uprintf("Virtual Memory Destroyed! \n");
	  break;
	default:
	  error = EOPNOTSUPP;
	  break;
	}
	return error;
}

SYSCALL_MODULE(kmalloc,&offset, &kmalloc_sysent, load, NULL);


