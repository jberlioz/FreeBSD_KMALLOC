#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>

static int event_handler(struct module *module, int event, void *arg)
{
	int e = 0;
	
	switch(event)
	{
	case MOD_LOAD:
		uprintf("Memory allocated!");
		break;
	case MOD_UNLOAD:
		uprintf("Deallocating!");
		break;
	default:
		e = EOPNOTSUPP;
		break;
	}
	return (e);
}

static moduledata_t kmalloc_conf=
{
	"kmallo_fsm",
	event_handler,
	NULL
};

DECLARE_MODULE(kmalloc_fsm, kmalloc_conf, SI_SUB_DRIVERS, 
SI_ORDER_MIDDLE);
