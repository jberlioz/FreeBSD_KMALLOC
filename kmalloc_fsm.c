#include <sys/param.h>
#include <sys/module.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/file.h>
#include <sys/malloc.h>


static void *addr_p;
void kmalloc(unsigned long, int);
void kmem_free(void *);

//MALLOC_DEFINE(M_BUF1, "M_BUF1", "struct");
//struct buffer_ent *buf_1, *buf_2;
//MALLOC(m_buf, struct buffer_ent, tmp, M_BUF1); 
MALLOC_DECLARE(kmalloc_buf);
MALLOC_DEFINE(kmalloc_buf, "kmalloc", "buffer for kmalloc");

static int event_handler(struct module *module, int event, void *arg)
{
	int e = 0;
	
	switch(event)
	{
	case MOD_LOAD:
	  kmalloc(4,M_NODUMP|M_NOWAIT);
	  uprintf("Memory allocated!");
	  break;
	case MOD_UNLOAD:
	  kmem_free(addr_p);
	  uprintf("Deallocating!");
	  break;
	default:
	  e = EOPNOTSUPP;
		break;
	}
	return (e);
}

static  moduledata_t kmalloc_conf=
{
	"kmallo_fsm",
	event_handler,
	NULL
};




void kmalloc(unsigned long size, int flags)
{
  addr_p = malloc(size, kmalloc_buf, flags);
  uprintf("%p\n",addr_p);
  KASSERT(addrp == NULL, "Allocation failed!");
}

void kmem_free(void *addr)
{
  free(addr,kmalloc_buf);
}
 
DECLARE_MODULE(kmalloc_fsm, kmalloc_conf, SI_SUB_DRIVERS, 
SI_ORDER_MIDDLE);
