
#include <sys/types.h>
#include <sys/module.h>
#include <sys/syscall.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static void kmalloc();
#define KMALLOC 0
#define KFREE 1

int
main(int argc __unused, char *argv[])
{
	int modid, syscall_num;
	struct module_stat stat;

	stat.version = sizeof(stat);
	if ((modid = modfind("sys/kmalloc")) == -1)
		err(1, "modfind");
	if (modstat(modid, &stat) != 0)
		err(1, "modstat");
	syscall_num = stat.data.intval;
	
	void *addr;
	void *addrOld;

	size_t val = 4;
	
	int cmd = atoi(argv[1]);
	printf("%d",cmd);
	switch (cmd)
		{
		case KMALLOC:
			syscall(syscall_num,0, val, addr);
			break;
		case KFREE:
			syscall(syscall_num,1, val, addr);
			break;
		default:
			break;
		}
}
